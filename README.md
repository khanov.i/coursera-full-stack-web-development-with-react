# Coursera Full-Stack Web Development with React

'Full-Stack Web Development with React' course on Coursera.

Here will be exercises' files storage during course progress.


🐉 Must be **completely** useless for you. Please enroll in the [course](https://www.coursera.org/specializations/full-stack-react) instead.

```mermaid
journey
    title Course flow
    section Сourse mastering
      Watch video: 5: Mr.Proper
      Read notes: 3: Me, Mr.Proper
      Read docs: 4: Me, Mr.Proper
      Do exercise: 7: Me, Mr.Proper
    section Home diversion
      Go upstairs: 5: Me
      Sit down: 5: Me
      Code something else: 7: Me, Mr.Proper
```
